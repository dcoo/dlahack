from flask import Flask, request, make_response, jsonify
import requests
import json
import pdb

userid = 'glsTBiZOo9k4a8B'
api_token = 's9nv43sexKI5/OkEF2+imMbpWtt2YhR92J2wlpfjvF4kACU2qLqP0hJ9Y/I2xEhPxdZNSF4FxEZMV87iEEbR1w=='
headers = {"Authorization": "Bearer " + api_token }

def parse_skills(data):
    skill_gaps = data['OccupationSkillsGapList']
    skill_matches = data['OccupationSkillsMatchList']
    skills_to_acquire = {}
    for skill in skill_gaps:
        # Very High means that you would have to study a long time to learn the skill
        if skill['AvailableTraining'] and skill['SKScoreTarget'] in ['Very Low', 'Low', 'Medium']:   # Very Low, Low, Medium, High, Very High
            skills_to_acquire[skill['SkillId']] = skill

    return (skill_gaps, skills_to_acquire, skill_matches)

def make_gap_url(current_job_code, target_job_code, cityorstate):
    return 'https://api.careeronestop.org/v1/skillgap/' + userid + '/' + current_job_code + '/' + target_job_code + '/' + cityorstate + '/25'

def make_training_url(skill_id, cityorstate):
    return 'https://api.careeronestop.org/v1/training/' + userid + '/skill/' + skill_id + '/' + cityorstate + '/25/0/0/0/0/0/0/0/0/5'

def get_skill_gaps(current_job_code, target_job_code, cityorstate):
    url = make_gap_url(current_job_code, target_job_code, cityorstate)
    gaps = requests.get(url, headers=headers)
    skill_gaps, skills_to_acquire, skill_matches = parse_skills(gaps.json())

    skill_ids = [k for k in skills_to_acquire.keys()]

    # limit skill ids
    if len(skill_ids) > 3:
        sids = skill_ids[:3]

    trainings = [requests.get(make_training_url(sid, cityorstate), headers=headers).json()['SchoolPrograms'] for sid in sids]
    schools = []
    for t in trainings:
        schools.append(t[0])

    fulfillment_messages = []
    fulfillment_messages.append(
        { 
            "text": { 
                "text" : [ "You have %s skill(s) in common with your target job and %s new skill(s) left to learn" % (len(skill_matches), len(skill_gaps)) ] 
            } 
        }
    )

    fulfillment_messages.append(
        { 
            "text": { 
                "text" : [ "These are school programs which have classes on new skills most closely related to your current job:" ] 
            } 
        }
    )

    for s in schools:
        sid = s['ElementID']
        if '|' in sid:
            sid = sid[sid.rfind('|')+1:]

        if sid in skills_to_acquire:
            fulfillment_messages.append(
                { 
                    "text": { 
                        "text" : [ "'%s' has a program for '%s' which teaches '%s'" % (s['SchoolName'], s['ProgramName'], skills_to_acquire[sid]['Description']) ] 
                    } 
                }
            )
        else:
            fulfillment_messages.append(
                { 
                    "text": { 
                        "text" : [ "'%s' has a program for '%s'" % (s['SchoolName'], s['ProgramName']) ] 
                    } 
                }
            )
    
    return fulfillment_messages

def main(request):
    if request.method == 'POST':
        data = request.get_json()
        context_params = {}
        for context in data.get('queryResult').get('outputContexts'):
            if 'jobprocess-job1' in context['name']:
                context_params = context['parameters']
        current_job_code = context_params['currentJob.original']
        target_job_code = context_params['futureJob.original']
        cityorstate = context_params['state.original']
        fulfillment_messages = get_skill_gaps(current_job_code, target_job_code, cityorstate)

        response_object = {
            "fulfillmentText" : "Calculating skill gaps...",
            "fulfillmentMessages" : fulfillment_messages,
            "source": ""
        }

        return jsonify(response_object)

    elif request.method == 'GET':
        return 'Please send job codes and state'
